﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EmergencyX.CacheClearing
{
    public class CacheClearing
    {
	public static void clearCache(string pathToClear)
	{
			DirectoryInfo dir = new DirectoryInfo(pathToClear);
			foreach (FileInfo file in dir.GetFiles())
			{
				file.IsReadOnly = false;
				file.Delete();
			}
			
			foreach(DirectoryInfo dirs in dir.GetDirectories())
			{
				//cleare sub dirs
				//
				clearCache(dirs.FullName);
				dirs.Delete();
			}
	}
    }
}
