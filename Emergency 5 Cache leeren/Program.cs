﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using EmergencyX.CacheClearing;

namespace Emergency_5_Cache_leeren
{
	class DeleteCache
	{
		static void Main(string[] args)
		{
			string path;

			//if the first argument is not specified, use a default value
			//
			if(args.Length == 0)
			{
				path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Promotion Software GmbH\EMERGENCY 5\cache\";
			}
			else //elsewise use the given argument
			{
				path = args[0];
			}

			//Startup
			//
			Console.WriteLine("Emergency X - Emergency 5 Cache Clearing {0}", typeof(Emergency_5_Cache_leeren.DeleteCache).Assembly.GetName().Version);

			//Some nice white space
			//
			Console.WriteLine("");

			//Information to users...
			//
			Console.WriteLine("Der Emergency 5/2016/2017/20 (Years) Cache wird geleert...");
			Console.WriteLine("Verwendeter Pfad: {0}", path);
			
			try {
				CacheClearing.clearCache(path);
			}
			catch(System.IO.DirectoryNotFoundException dnfe)
			{
				Console.WriteLine("Ein Problem ist aufgetreten. Der Pfad konnte nicht gefunden werden!");
				Console.WriteLine("Fehler: {0}", dnfe.Message);
				Console.WriteLine("Stactrace: {0}", dnfe.StackTrace);

				//Close Program
				Console.WriteLine("Drücken Sie eine beliebige Taste zum Beenden des Programms.");
				Console.Read(); // Stop the program so that the user can read our informations
				Environment.Exit(-1);
			}
			catch(System.IO.DriveNotFoundException drnfe)
			{
				Console.WriteLine("Das Laufwerk konnte nicht gefunden oder geöffnet werden!");
				Console.WriteLine("Fehler: {0}", drnfe.Message);
				Console.WriteLine("Stactrace: {0}", drnfe.StackTrace);

				//Close Program
				Console.WriteLine("Drücken Sie eine beliebige Taste zum Beenden des Programms.");
				Console.Read(); // Stop the program so that the user can read our informations
				Environment.Exit(-1);
			}
			catch(Exception e)
			{
				Console.WriteLine("Ein Fehler ist aufgetreten.");
				Console.WriteLine("Wenden Sie sich unter www.emergency-forum.de mit nachfolgenden Informationen an ciajoe");
				Console.WriteLine("Fehler: {0}", e.Message);
				Console.WriteLine("Stactrace: {0}", e.StackTrace);
				
				//Close Program
				Console.WriteLine("Drücken Sie eine beliebige Taste zum Beenden des Programms.");
				Console.Read(); // Stop the program so that the user can read our informations
				Environment.Exit(-1);
			}

			//All nicely done...
			//
			Console.WriteLine("Der Cache wurde erfolgreich gelöscht. Drücken Sie eine beliebige Taste zum Beenden des Programms.");
			Console.Read(); // Stop the program so that the user can read our informations

		}
	}
}
